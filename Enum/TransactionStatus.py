from enum import Enum


class TransactionStatus(Enum):
    OPEN = 'Mới tạo'
    INPROGRESS = 'Đang giao dịch'
    OVERTIME = 'Quá hạn'
    CLOSED = 'Đóng'
