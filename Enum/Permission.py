from enum import Enum


class Permission(Enum):
    SUPER = 1
    ADMINISTRATOR = 2
    LIBERIAN = 3
