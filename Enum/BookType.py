from enum import Enum


class BookType(Enum):
    NONFICTION = 'Non - fiction'
    COMIC = 'Truyện tranh'
    NOVEL = 'Tiểu thuyết'
    SCIENCE = 'Khoa học'
    TEXTBOOK = 'Textbook'
    SELF_HELP = 'Self-help'
    TECH = 'Công nghệ'
