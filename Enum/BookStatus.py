from enum import Enum


class BookStatus(Enum):
    NEW = 'Mới'
    USING = 'Đang được mượn'
    LOST = 'Bị mất'
    BROKEN = 'Bị hỏng'
    REMOVED = 'Đã bỏ'
