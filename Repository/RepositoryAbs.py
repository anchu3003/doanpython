from abc import abstractmethod
from typing import TypeVar, Generic

from Utils.DbConnection import DbConnection

T = TypeVar('T')


class RepositoryAbs(Generic[T]):
    def __init__(self):
        self.connection = DbConnection()

    @abstractmethod
    def insert(self, new: T) -> bool:
        pass

    @abstractmethod
    def delete(self, Id: int) -> bool:
        pass

    @abstractmethod
    def update(self, obj: T) -> bool:
        pass

    @abstractmethod
    def getByName(self, Id: int) -> T | None:
        pass

    @abstractmethod
    def getAll(self, obj: T = None) -> []:
        pass
