from Class.Book import Book
from Repository.AuthorRepository import AuthorRepository
from Repository.RepositoryAbs import RepositoryAbs


class BookRepository(RepositoryAbs[Book]):
    def __init__(self):
        super().__init__()

    def insert(self, new: Book) -> bool:
        sql = (
            """Insert into book (id, name, book_type, publish_year, publisher, author_id, buy_date, status) 
            values ({0},'{1}','{2}',{3},'{4}',{5},'{6}','{7}')"""
            .format(new.id, new.name, new.bookType, new.publishYear, new.publisher, new.authorId,
                    new.buyDate, new.status))
        return self.connection.query(sql)

    def delete(self, Id: int) -> bool:
        sql = """Delete from book Where book.id = {0}""".format(Id)
        return self.connection.query(sql)

    def update(self, obj: Book) -> bool:
        if obj.id:
            sql = "Update book set book.id = " + str(obj.id)
            if obj.name:
                sql += ", book.name = '" + obj.name + "'"
            if obj.bookType:
                sql += ", book.book_type = '" + str(obj.bookType) + "'"
            if obj.publishYear:
                sql += ", book.publish_year = " + str(obj.publishYear)
            if obj.publisher:
                sql += ", book.publisher = '" + obj.publisher + "'"
            if obj.authorId:
                sql += ", book.author_id = " + str(obj.authorId)
            if obj.buyDate:
                sql += ", book.buy_date = '" + str(obj.buyDate) + "'"
            if obj.status:
                sql += ", book.status = '" + str(obj.status) + "'"
            sql += " where book.id = " + str(obj.id)
            return self.connection.query(sql)
        return False

    def getByName(self, bookId: int) -> Book | None:
        sql = """Select * from book Where book.id = """ + int(bookId)
        result = self.connection.result_set(sql)
        book = None
        if len(result) > 0:
            book = Book(result[0][0], result[0][1], result[0][2], result[0][3], result[0][4], result[0][5],
                        result[0][6], result[0][7])
        return book



    def getId(self, bookName: str) -> Book | None:
        sql = "Select * from book Where book.name = N'" + bookName + "'"
        result = self.connection.result_set(sql)
        book = None
        if len(result) > 0:
            book = Book(result[0][0], result[0][1], result[0][2], result[0][3], result[0][4], result[0][5],
                        result[0][6], result[0][7])
        return book

    def getAll(self, obj: Book = None) -> []:
        sql = """Select * from book"""
        if bool(obj):
            sql += " where 1=1 "
            if obj.id:
                sql += " and book.id = " + str(obj.id)
            if obj.name:
                sql += " and book.name = N'" + obj.name + "'"
            if obj.bookType:
                sql += " and book.book_type = '" + str(obj.bookType) + "'"
            if obj.publishYear:
                sql += " and book.publish_year = " + str(obj.publishYear)
            if obj.publisher:
                sql += " and book.publisher = '" + obj.publisher + "'"
            if obj.authorId:
                sql += " and book.author_id = " + str(obj.authorId)
            if obj.buyDate:
                sql += " and book.buy_date = '" + str(obj.buyDate) + "'"
            if obj.status:
                sql += " and book.status = '" + str(obj.status) + "'"

        results = self.connection.result_set(sql)
        a = AuthorRepository()
        books = []
        for result in results:
            if result[5]:
                author = a.get(int(result[5]))
                if author:
                    result[5] = str(author.name)
            book = Book(result[0], result[1], result[2], result[3], result[4], result[5], result[6], result[7])
            books.extend([book])
        return books
