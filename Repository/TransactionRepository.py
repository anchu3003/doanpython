from Class.Book import Book
from Class.Transaction import Transaction
from Repository.AuthorRepository import AuthorRepository
from Repository.RepositoryAbs import RepositoryAbs


class TransactionRepository(RepositoryAbs[Transaction]):
    def __init__(self):
        super().__init__()

    def insert(self, new: Transaction) -> bool:
        sql = (
            """Insert into [transaction] (id, account_id, borrowed_date, return_date, status) 
            values ({0},{1},'{2}','{3}','{4}')"""
            .format(new.id, new.accountId, new.borrowedDate, new.returnDate, new.status))
        self.connection.query(sql)
        for bookId in new.bookIds:
            sql = """Insert into transaction_book (transaction_id, book_id) 
            values ({0},{1})""".format(new.id, bookId)
            self.connection.query(sql)
        return True

    # def delete(self, Id: int) -> bool:
    #     sql = """Delete from transaction Where transaction.id = {0}""".format(Id)
    #     return self.connection.query(sql)
    #
    def update(self, obj: Transaction) -> bool:
        if obj.id:
            sql = "Update [transaction] set [transaction].id = " + str(obj.id)
            if obj.status:
                sql += " ,[transaction].status = '" + str(obj.status) + "'"
            sql += " where [transaction].id = " + str(obj.id)
            return self.connection.query(sql)
        return False

    def getByName(self, Id: int) -> Transaction | None:
        sql = """Select * from [transaction] Where [transaction].id = """ + str(Id)
        result = self.connection.result_set(sql)
        transaction = None
        if len(result) > 0:
            transaction = Transaction(result[0][0], result[0][1], result[0][2], result[0][3], result[0][4])

        sql = """Select * from book join transaction_book on book.id = transaction_book.book_id
                Where transaction_book.transaction_id = """ + str(Id)
        results = self.connection.result_set(sql)
        a = AuthorRepository()
        books = []
        for rs in results:
            if rs[5]:
                author = a.getByName(str(rs[5]))
                if author:
                    rs[5] = str(author.name)
            book = Book(rs[0], rs[1], rs[2], rs[3], rs[4], rs[5], rs[6], rs[7])
            books.extend([book])
        transaction.books = books
        return transaction

    def getAll(self, obj: Transaction = None) -> []:
        sql = """Select * from [transaction]"""
        if bool(obj):
            sql += " where 1=1 "
            if obj.accountId:
                sql += " and [transaction].account_id = " + str(obj.accountId)
            if obj.borrowedDate:
                sql += " and [transaction].borrowed_date = '" + str(obj.borrowedDate) + "'"
            if obj.returnDate:
                sql += " and [transaction].return_date = '" + str(obj.returnDate) + "'"
            if obj.status:
                sql += " and [transaction].status = '" + str(obj.status) + "'"
            if obj.id:
                sql += " and [transaction].id = " + str(obj.id)

        results = self.connection.result_set(sql)
        transactions = []
        for result in results:
            transaction = Transaction(result[0], result[1], result[2], result[3], result[4])
            transactions.extend([transaction])
        return transactions
