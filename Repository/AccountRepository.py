from Class.Account import Account
from Repository.RepositoryAbs import RepositoryAbs


class AccountRepository(RepositoryAbs[Account]):
    def __init__(self):
        super().__init__()

    def insert(self, new: Account) -> bool:
        sql = (
            """Insert into account (id, password, name, birthday, address, email, phone, permission) 
            values ({0},'{1}','{2}','{3}','{4}','{5}','{6}','{7}')"""
            .format(new.id, new.password, new.name, new.birthday, new.address, new.email,
                    new.phoneNo, new.permission))
        return self.connection.query(sql)

    def delete(self, Id: int) -> bool:
        sql = """Delete from account Where account.id = {0}""".format(Id)
        return self.connection.query(sql)

    def update(self, obj: Account) -> bool:
        if obj.id:
            sql = "Update account set account.id = " + str(obj.id)
            if obj.name:
                sql += " , account.name = '" + obj.name + "'"
            if obj.birthday:
                sql += " , account.birthday = '" + str(obj.birthday) + "'"
            if obj.address:
                sql += " , account.address = '" + obj.address + "'"
            if obj.email:
                sql += " , account.email = '" + obj.email + "'"
            if obj.phoneNo:
                sql += " , account.phone = '" + obj.phoneNo + "'"
            if obj.password:
                sql += " , account.password = '" + obj.password + "'"
            if obj.permission:
                sql += " , account.permission = " + str(obj.permission)
            sql += " where account.id = " + str(obj.id)
            return self.connection.query(sql)
        return False

    def getByName(self, Id: int) -> Account | None:
        sql = """Select * from account Where account.id = """ + str(Id)
        result = self.connection.result_set(sql)
        account = None
        if len(result) > 0:
            account = Account(result[0][0], result[0][1], result[0][2], result[0][3], result[0][4], result[0][5],
                              result[0][6], result[0][7])
        return account

    def getAll(self, obj: Account = None) -> []:
        sql = """Select id, password, name, birthday, address, email, phone from account"""

        if bool(obj):
            sql += " where 1=1 "
            if obj.id:
                sql += " and account.id = " + str(obj.id)
            if obj.name:
                sql += " and account.name = '" + obj.name + "'"
            if obj.birthday:
                sql += " and account.birthday = '" + str(obj.birthday) + "'"
            if obj.address:
                sql += " and account.address = '" + obj.address + "'"
            if obj.email:
                sql += " and account.email = '" + obj.email + "'"
            if obj.phoneNo:
                sql += " and account.phone = '" + obj.phoneNo + "'"
            if obj.permission:
                sql += " and account.permission = " + str(obj.permission)
        results = self.connection.result_set(sql)
        accounts = []
        for result in results:
            account = Account(result[0], result[1], result[2], result[3], result[4], result[5],
                              result[6])
            accounts.extend([account])
        return accounts
