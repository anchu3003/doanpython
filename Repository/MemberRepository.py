from Class.Member import Member
from Repository.RepositoryAbs import RepositoryAbs


class MemberRepository(RepositoryAbs[Member]):
    def __init__(self):
        super().__init__()

    def insert(self, new: Member) -> bool:
        sql = (
            """Insert into member (id, name, birthday, address, email, phone, ngay_lap_the, debt) 
            values ({0},'{1}','{2}','{3}','{4}','{5}','{6}','{7}')"""
            .format(new.id, new.name, new.birthday, new.address, new.email,
                    new.phoneNo, new.ngayLapThe, new.debt))
        return self.connection.query(sql)

    def delete(self, Id: int) -> bool:
        sql = """Delete from member Where member.id = {0}""".format(Id)
        return self.connection.query(sql)

    def update(self, obj: Member) -> bool:
        if obj.id:
            sql = "Update member set member.id = " + str(obj.id)
            if obj.name:
                sql += " , member.name = '" + obj.name + "'"
            if obj.birthday:
                sql += " , member.birthday = '" + str(obj.birthday) + "'"
            if obj.address:
                sql += " , member.address = '" + obj.address + "'"
            if obj.email:
                sql += " , member.email = '" + obj.email + "'"
            if obj.phoneNo:
                sql += " , member.phone = '" + obj.phoneNo + "'"
            if obj.ngayLapThe:
                sql += " , member.ngay_lap_the = '" + obj.ngayLapThe + "'"
            if obj.debt:
                sql += " , member.debt = " + str(obj.debt)
            sql += " where member.id = " + str(obj.id)
            return self.connection.query(sql)
        return False

    def getByName(self, Id: int) -> Member | None:
        sql = """Select * from member Where member.id = """ + str(Id)
        result = self.connection.result_set(sql)
        member = None
        if len(result) > 0:
            member = Member(result[0][0], result[0][1], result[0][2], result[0][3], result[0][4], result[0][5],
                            result[0][6], result[0][7])
        return member

    def getAll(self, obj: Member = None) -> []:
        sql = """Select id, name, birthday, address, email, phone, ngay_lap_the, debt from member"""
        if bool(obj):
            sql += " where 1=1 "
            if obj.id:
                sql += " and member.id = " + str(obj.id)
            if obj.name:
                sql += " and member.name = '" + obj.name + "'"
            if obj.birthday:
                sql += " and member.birthday = '" + str(obj.birthday) + "'"
            if obj.address:
                sql += " and member.address = '" + obj.address + "'"
            if obj.email:
                sql += " and member.email = '" + obj.email + "'"
            if obj.phoneNo:
                sql += " and member.phone = '" + obj.phoneNo + "'"
            if obj.ngayLapThe:
                sql += " and member.ngay_lap_the = '" + obj.ngayLapThe + "'"
            if obj.debt:
                sql += " and member.debt = " + str(obj.debt)
        results = self.connection.result_set(sql)
        members = []
        for result in results:
            member = Member(result[0], result[1], result[2], result[3], result[4], result[5],
                            result[6], result[7])
            members.extend([member])
        return members
