from Class.Author import Author
from Repository.RepositoryAbs import RepositoryAbs


class AuthorRepository(RepositoryAbs[Author]):
    def __init__(self):
        super().__init__()

    def insert(self, new: Author) -> bool:
        sql = (
            """Insert into author (id, name) values ({0},N'{1}')""".format(new.id, new.name))
        return self.connection.query(sql)

    def delete(self, Id: int) -> bool:
        sql = """Delete from author Where author.id = {0}""".format(Id)
        return self.connection.query(sql)

    def update(self, obj: Author) -> bool:
        if obj.id:
            sql = "Update author set author.id = " + str(obj.id)
            if obj.name:
                sql += " , author.name = N'" + obj.name + "'"
            sql += " where author.id = " + str(obj.id)
            return self.connection.query(sql)
        return False

    def getByName(self, authorName: str) -> Author | None:
        sql = "SELECT * FROM author WHERE author.name = N'" + authorName + "'"
        result = self.connection.result_set(sql)
        account = None
        if len(result) > 0:
            account = Author(result[0][0], result[0][1])
        return account

    def get(self, authorId: int) -> Author | None:
        sql = "SELECT * FROM author WHERE author.id = " + str(authorId) + ""

        result = self.connection.result_set(sql)
        account = None
        if len(result) > 0:
            account = Author(result[0][0], result[0][1])
        return account

    def getAll(self, obj: Author = None) -> []:
        sql = """Select * from author"""
        if bool(obj):
            sql += " where 1=1 "
            if obj.id:
                sql += " and author.id = " + str(obj.id)
            if obj.name:
                sql += " and author.name = N'" + obj.name + "'"

        results = self.connection.result_set(sql)
        authors = []
        for result in results:
            author = Author(result[0], result[1])
            authors.extend([author])
        return authors

    def getAllAuthorName(self) -> list[str]:
        list = []
        sql = """SELECT name FROM author"""
        results = self.connection.result_set(sql)
        for result in results:
            list.append(result[0])
        # nameList = [str(value).strip() for value in list]
        return list
