USE [master]
GO
/****** Object:  Database [QLTV]    Script Date: 14/01/2024 7:37:21 CH ******/
CREATE DATABASE [QLTV]
 CONTAINMENT = NONE
 ON  PRIMARY
( NAME = N'QLTV', FILENAME = N'D:\Programs\SQL Express\MSSQL16.SQLEXPRESS02\MSSQL\DATA\QLTV.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON
( NAME = N'QLTV_log', FILENAME = N'D:\Programs\SQL Express\MSSQL16.SQLEXPRESS02\MSSQL\DATA\QLTV_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT, LEDGER = OFF
GO
ALTER DATABASE [QLTV] SET COMPATIBILITY_LEVEL = 160
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [QLTV].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [QLTV] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [QLTV] SET ANSI_NULLS OFF
GO
ALTER DATABASE [QLTV] SET ANSI_PADDING OFF
GO
ALTER DATABASE [QLTV] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [QLTV] SET ARITHABORT OFF
GO
ALTER DATABASE [QLTV] SET AUTO_CLOSE ON
GO
ALTER DATABASE [QLTV] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [QLTV] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [QLTV] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [QLTV] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [QLTV] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [QLTV] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [QLTV] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [QLTV] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [QLTV] SET  DISABLE_BROKER
GO
ALTER DATABASE [QLTV] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [QLTV] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [QLTV] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [QLTV] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [QLTV] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [QLTV] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [QLTV] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [QLTV] SET RECOVERY SIMPLE
GO
ALTER DATABASE [QLTV] SET  MULTI_USER
GO
ALTER DATABASE [QLTV] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [QLTV] SET DB_CHAINING OFF
GO
ALTER DATABASE [QLTV] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF )
GO
ALTER DATABASE [QLTV] SET TARGET_RECOVERY_TIME = 60 SECONDS
GO
ALTER DATABASE [QLTV] SET DELAYED_DURABILITY = DISABLED
GO
ALTER DATABASE [QLTV] SET ACCELERATED_DATABASE_RECOVERY = OFF
GO
ALTER DATABASE [QLTV] SET QUERY_STORE = ON
GO
ALTER DATABASE [QLTV] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [QLTV]
GO
/****** Object:  Table [dbo].[account]    Script Date: 14/01/2024 7:37:21 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[account](
	[id] [int] NOT NULL,
	[password] [nvarchar](50) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[birthday] [nvarchar](50) NULL,
	[address] [nvarchar](50) NULL,
	[email] [nvarchar](50) NULL,
	[phone] [nvarchar](12) NULL,
	[permission] [int] NOT NULL,
 CONSTRAINT [PK_account] PRIMARY KEY CLUSTERED
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[author]    Script Date: 14/01/2024 7:37:21 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[author](
	[id] [int] NOT NULL,
	[name] [nvarchar](500) NOT NULL,
 CONSTRAINT [PK_author] PRIMARY KEY CLUSTERED
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[book]    Script Date: 14/01/2024 7:37:21 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[book](
	[id] [int] NOT NULL,
	[name] [nvarchar](50) NULL,
	[book_type] [nvarchar](50) NULL,
	[publish_year] [int] NULL,
	[publisher] [nvarchar](50) NULL,
	[author_id] [int] NULL,
	[buy_date] [nvarchar](50) NULL,
	[status] [nvarchar](50) NULL,
 CONSTRAINT [PK_book] PRIMARY KEY CLUSTERED
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[member]    Script Date: 14/01/2024 7:37:21 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[member](
	[id] [int] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[birthday] [nvarchar](50) NULL,
	[address] [nvarchar](50) NULL,
	[email] [nvarchar](50) NULL,
	[phone] [nvarchar](12) NULL,
	[ngay_lap_the] [nvarchar](10) NULL,
	[debt] [int] NULL,
 CONSTRAINT [PK_member] PRIMARY KEY CLUSTERED
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[transaction]    Script Date: 14/01/2024 7:37:21 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[transaction](
	[id] [int] NOT NULL,
	[account_id] [int] NULL,
	[borrowed_date] [nvarchar](50) NULL,
	[return_date] [nvarchar](50) NULL,
	[status] [nvarchar](50) NULL,
 CONSTRAINT [PK_transaction] PRIMARY KEY CLUSTERED
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[transaction_book]    Script Date: 14/01/2024 7:37:21 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[transaction_book](
	[id] [int] NOT NULL IDENTITY(1,1),
	[transaction_id] [int] NOT NULL,
	[book_id] [int] NOT NULL,
 CONSTRAINT [PK_transaction_book] PRIMARY KEY CLUSTERED
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[book]  WITH CHECK ADD  CONSTRAINT [FK_book_author] FOREIGN KEY([author_id])
REFERENCES [dbo].[author] ([id])
GO
ALTER TABLE [dbo].[book] CHECK CONSTRAINT [FK_book_author]
GO
ALTER TABLE [dbo].[transaction]  WITH CHECK ADD  CONSTRAINT [FK_transaction_member] FOREIGN KEY([account_id])
REFERENCES [dbo].[member] ([id])
GO
ALTER TABLE [dbo].[transaction] CHECK CONSTRAINT [FK_transaction_member]
GO
ALTER TABLE [dbo].[transaction_book]  WITH CHECK ADD  CONSTRAINT [FK_transaction_book_book] FOREIGN KEY([book_id])
REFERENCES [dbo].[book] ([id])
GO
ALTER TABLE [dbo].[transaction_book] CHECK CONSTRAINT [FK_transaction_book_book]
GO
ALTER TABLE [dbo].[transaction_book]  WITH CHECK ADD  CONSTRAINT [FK_transaction_book_transaction] FOREIGN KEY([transaction_id])
REFERENCES [dbo].[transaction] ([id])
GO
ALTER TABLE [dbo].[transaction_book] CHECK CONSTRAINT [FK_transaction_book_transaction]
GO
USE [master]
GO
ALTER DATABASE [QLTV] SET  READ_WRITE
GO


--Insert account
INSERT [dbo].[account] ([id], [password], [name], [birthday], [address], [email], [phone], [permission])
VALUES ('1', '12345678', 'Nguyen Van A1', '1994-01-01', '', '', '', 1)
INSERT [dbo].[account] ([id], [password], [name], [birthday], [address], [email], [phone], [permission])
VALUES ('2', '12345678', 'Nguyen Van A2', '1994-01-01', '', '', '', 2)
INSERT [dbo].[account] ([id], [password], [name], [birthday], [address], [email], [phone], [permission])
VALUES ('3', '12345678', 'Nguyen Van A3', '1994-01-01', '', '', '', 3)
INSERT [dbo].[account] ([id], [password], [name], [birthday], [address], [email], [phone], [permission])
VALUES ('4', '12345678', 'Nguyen Van A4', '1994-01-01', '', '', '', 2)
INSERT [dbo].[account] ([id], [password], [name], [birthday], [address], [email], [phone], [permission])
VALUES ('5', '12345678', 'Nguyen Van A5', '1994-01-01', '', '', '', 3)

INSERT [dbo].[member] ([id], [name], [birthday], [address], [email], [phone], [ngay_lap_the], [debt])
VALUES (6, 'Nguyen Van A1','1994-01-01','TPHCM','a@hotmail.com','0123456789','2020-01-01', 0)
INSERT [dbo].[member] ([id], [name], [birthday], [address], [email], [phone], [ngay_lap_the], [debt])
VALUES (7, 'Nguyen Van A2','1994-01-01','TPHCM','a@hotmail.com','0123456789','2020-01-01', 0)
INSERT [dbo].[member] ([id], [name], [birthday], [address], [email], [phone], [ngay_lap_the], [debt])
VALUES (8, 'Nguyen Van A3','1994-01-01','TPHCM','a@hotmail.com','0123456789','2020-01-01', 0)
INSERT [dbo].[member] ([id], [name], [birthday], [address], [email], [phone], [ngay_lap_the], [debt])
VALUES (9, 'Nguyen Van A4','1994-01-01','TPHCM','a@hotmail.com','0123456789','2020-01-01', 0)
INSERT [dbo].[member] ([id], [name], [birthday], [address], [email], [phone], [ngay_lap_the], [debt])
VALUES (10, 'Nguyen Van A5','1994-01-01','TPHCM','a@hotmail.com','0123456789','2020-01-01', 0)
INSERT [dbo].[member] ([id], [name], [birthday], [address], [email], [phone], [ngay_lap_the], [debt])
VALUES (11, 'Nguyen Van A6','1994-01-01','TPHCM','a@hotmail.com','0123456789','2020-01-01', 0)
INSERT [dbo].[member] ([id], [name], [birthday], [address], [email], [phone], [ngay_lap_the], [debt])
VALUES (12, 'Nguyen Van A7','1994-01-01','TPHCM','a@hotmail.com','0123456789','2020-01-01', 0)
INSERT [dbo].[member] ([id], [name], [birthday], [address], [email], [phone], [ngay_lap_the], [debt])
VALUES (13, 'Nguyen Van A8','1994-01-01','TPHCM','a@hotmail.com','0123456789','2020-01-01', 0)
INSERT [dbo].[member] ([id], [name], [birthday], [address], [email], [phone], [ngay_lap_the], [debt])
VALUES (14, 'Nguyen Van A9','1994-01-01','TPHCM','a@hotmail.com','0123456789','2020-01-01', 0)
INSERT [dbo].[member] ([id], [name], [birthday], [address], [email], [phone], [ngay_lap_the], [debt])
VALUES (15, 'Nguyen Van A10','1994-01-01','TPHCM','a@hotmail.com','0123456789','2020-01-01', 0)
INSERT [dbo].[member] ([id], [name], [birthday], [address], [email], [phone], [ngay_lap_the], [debt])
VALUES (16, 'Nguyen Van A11','1994-01-01','TPHCM','a@hotmail.com','0123456789','2020-01-01', 0)
INSERT [dbo].[member] ([id], [name], [birthday], [address], [email], [phone], [ngay_lap_the], [debt])
VALUES (17, 'Nguyen Van A12','1994-01-01','TPHCM','a@hotmail.com','0123456789','2020-01-01', 0)
INSERT [dbo].[member] ([id], [name], [birthday], [address], [email], [phone], [ngay_lap_the], [debt])
VALUES (18, 'Nguyen Van A13','1994-01-01','TPHCM','a@hotmail.com','0123456789','2020-01-01', 0)
INSERT [dbo].[member] ([id], [name], [birthday], [address], [email], [phone], [ngay_lap_the], [debt])
VALUES (19, 'Nguyen Van A14','1994-01-01','TPHCM','a@hotmail.com','0123456789','2020-01-01', 0)
INSERT [dbo].[member] ([id], [name], [birthday], [address], [email], [phone], [ngay_lap_the], [debt])
VALUES (20, 'Nguyen Van A15','1994-01-01','TPHCM','a@hotmail.com','0123456789','2020-01-01', 0)
INSERT [dbo].[member] ([id], [name], [birthday], [address], [email], [phone], [ngay_lap_the], [debt])
VALUES (21, 'Nguyen Van A16','1994-01-01','TPHCM','a@hotmail.com','0123456789','2020-01-01', 0)
INSERT [dbo].[member] ([id], [name], [birthday], [address], [email], [phone], [ngay_lap_the], [debt])
VALUES (22, 'Nguyen Van A17','1994-01-01','TPHCM','a@hotmail.com','0123456789','2020-01-01', 0)
INSERT [dbo].[member] ([id], [name], [birthday], [address], [email], [phone], [ngay_lap_the], [debt])
VALUES (23, 'Nguyen Van A18','1994-01-01','TPHCM','a@hotmail.com','0123456789','2020-01-01', 0)
INSERT [dbo].[member] ([id], [name], [birthday], [address], [email], [phone], [ngay_lap_the], [debt])
VALUES (24, 'Nguyen Van A19','1994-01-01','TPHCM','a@hotmail.com','0123456789','2020-01-01', 0)
INSERT [dbo].[member] ([id], [name], [birthday], [address], [email], [phone], [ngay_lap_the], [debt])
VALUES (25, 'Nguyen Van A20','1994-01-01','TPHCM','a@hotmail.com','0123456789','2020-01-01', 0)
INSERT [dbo].[member] ([id], [name], [birthday], [address], [email], [phone], [ngay_lap_the], [debt])
VALUES (26, 'Nguyen Van A21','1994-01-01','TPHCM','a@hotmail.com','0123456789','2020-01-01', 0)
INSERT [dbo].[member] ([id], [name], [birthday], [address], [email], [phone], [ngay_lap_the], [debt])
VALUES (27, 'Nguyen Van A22','1994-01-01','TPHCM','a@hotmail.com','0123456789','2020-01-01', 0)
INSERT [dbo].[member] ([id], [name], [birthday], [address], [email], [phone], [ngay_lap_the], [debt])
VALUES (28, 'Nguyen Van A23','1994-01-01','TPHCM','a@hotmail.com','0123456789','2020-01-01', 0)
INSERT [dbo].[member] ([id], [name], [birthday], [address], [email], [phone], [ngay_lap_the], [debt])
VALUES (29, 'Nguyen Van A24','1994-01-01','TPHCM','a@hotmail.com','0123456789','2020-01-01', 0)
INSERT [dbo].[member] ([id], [name], [birthday], [address], [email], [phone], [ngay_lap_the], [debt])
VALUES (30, 'Nguyen Van A25','1994-01-01','TPHCM','a@hotmail.com','0123456789','2020-01-01', 0)


--insert gia
INSERT [dbo].[author] ([id], [name])
VALUES (01, N'Huỳnh Tấn Khải')
INSERT [dbo].[author] ([id], [name])
VALUES (02, N'Nguyễn Thanh Nhã')
INSERT [dbo].[author] ([id], [name])
VALUES (03, N'Phan Quang Chiêu')
INSERT [dbo].[author] ([id], [name])
VALUES (04, N'Nguyễn Tấn Bảo Long')
INSERT [dbo].[author] ([id], [name])
VALUES (05, N'Nguyễn Khắc Xương')
INSERT [dbo].[author] ([id], [name])
VALUES (06, N'Nguyễn Vinh Dự')
INSERT [dbo].[author] ([id], [name])
VALUES (07, N'Nguyễn Thị Hồng Anh')
INSERT [dbo].[author] ([id], [name])
VALUES (08, N'Lê Tùng Lâm')
INSERT [dbo].[author] ([id], [name])
VALUES (09, N'Lê Thị Hằng Nga')
INSERT [dbo].[author] ([id], [name])
VALUES (10, N'Trần Nguyên Khang')
INSERT [dbo].[author] ([id], [name])
VALUES (11, N'Phạm Đi')
INSERT [dbo].[author] ([id], [name])
VALUES (12, N'Hà Minh Hồng chủ biên... [và những người khác] ')
INSERT [dbo].[author] ([id], [name])
VALUES (13, N'Nguyễn Thụy Phương')
INSERT [dbo].[author] ([id], [name])
VALUES (14, N'Dương Hoàng Lộc')
INSERT [dbo].[author] ([id], [name])
VALUES (15, N'Huỳnh Tuấn Kiệt, Nguyễn Văn Hiệu, Huỳnh Ngọc Thắng chủ biên')
INSERT [dbo].[author] ([id], [name])
VALUES (16, N'Derek Thompson ; Dương Hậu dịch ')
INSERT [dbo].[author] ([id], [name])
VALUES (17, N'Thomas Flynn ; Đinh Hồng Phúc dịch ')
INSERT [dbo].[author] ([id], [name])
VALUES (18, N'Bộ Giáo dục và Đào tạo')
INSERT [dbo].[author] ([id], [name])
VALUES (19, N'Đinh Ngọc Thạch, Doãn Chính đồng chủ biên')
INSERT [dbo].[author] ([id], [name])
VALUES (20, N'Graham Priest ; Nguyễn Văn Sướng dịch')
INSERT [dbo].[author] ([id], [name])
VALUES (21, N'Nguyễn Thanh Tùng')
INSERT [dbo].[author] ([id], [name])
VALUES (22, N'Stephen Mumford & Rani Lii Anjum ; Hoàng Phú Phương dịch ')
INSERT [dbo].[author] ([id], [name])
VALUES (23, N'Ngô Thị Mỹ Dung')
INSERT [dbo].[author] ([id], [name])
VALUES (24, N'Vũ Thị Thu Huyền')
INSERT [dbo].[author] ([id], [name])
VALUES (25, N'Henri Bergson ; Cao Văn Luận dịch')
INSERT [dbo].[author] ([id], [name])
VALUES (26, N'Lê Cung chủ biên ')
INSERT [dbo].[author] ([id], [name])
VALUES (27, N'Montesquieu ; Hoàng Thanh Đạm dịch.')
INSERT [dbo].[author] ([id], [name])
VALUES (28, N'Đinh Văn Quế')
INSERT [dbo].[author] ([id], [name])
VALUES (29, N'Nguyễn Ngọc Anh, Phan Trung Hoài chủ biên ')
INSERT [dbo].[author] ([id], [name])
VALUES (30, N'Nguyễn Hữu Phước')
INSERT [dbo].[author] ([id], [name])
VALUES (31, N'Cao Vũ Minh')
INSERT [dbo].[author] ([id], [name])
VALUES (32, N'Vũ Dương Huân')
INSERT [dbo].[author] ([id], [name])
VALUES (33, N'Đàng Quang Vắng')
INSERT [dbo].[author] ([id], [name])
VALUES (34, N'Nguyễn Thị Loan chủ biên ')
INSERT [dbo].[author] ([id], [name])
VALUES (35, N'Võ Văn Nhị chủ biên ... [và những người khác] ')
INSERT [dbo].[author] ([id], [name])
VALUES (36, N'Marion McGovern ; Lê Hồng Phương Hạ dịch ')
INSERT [dbo].[author] ([id], [name])
VALUES (37, N'Đinh Phi Hổ, Võ Văn Nhị, Trần Phước')
INSERT [dbo].[author] ([id], [name])
VALUES (38, N'Nguyễn Thị Loan chủ biên ')
INSERT [dbo].[author] ([id], [name])
VALUES (39, N'Tổng Cục thống kê.')
INSERT [dbo].[author] ([id], [name])
VALUES (40, N'Alistair Croll, Benjamin Yoskovitz ; Trần Mạnh Hoàng dịch ')
INSERT [dbo].[author] ([id], [name])
VALUES (41, N'Nguyễn Đình Bình')
INSERT [dbo].[author] ([id], [name])
VALUES (42, N'Randy Grieser ; Trịnh Huy Ninh dịch')
INSERT [dbo].[author] ([id], [name])
VALUES (43, N'Michael Bungay Stanier ; Đào Trung Uyên dịch ')
INSERT [dbo].[author] ([id], [name])
VALUES (44, N'Jeb Blount ; Vũ Thanh Tùng dịch')
INSERT [dbo].[author] ([id], [name])
VALUES (45, N'Michael R. Bloomberg ; Nguyễn Bình dịch ')
INSERT [dbo].[author] ([id], [name])
VALUES (46, N'Jamey Stegmaier ; Trần Minh Tuấn dịch ')
INSERT [dbo].[author] ([id], [name])
VALUES (47, N'Ngô Hải Minh... [và những người khác] ')
INSERT [dbo].[author] ([id], [name])
VALUES (48, N'David Goldwich; Nguyễn Ngô Hoài Linh dịch')
INSERT [dbo].[author] ([id], [name])
VALUES (49, N'Yumiko Kawanishi ; Nguyễn Thị Bích Huệ dịch ')
INSERT [dbo].[author] ([id], [name])
VALUES (50, N'Lolly Daskal ; Kim Phụng dịch ')


--insert sách
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (1, 'Book name 1', 'NOVEL', 2020, 'NXB Kim Dong', 1, '2020-02-01', 'NEW')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (2, 'Book name 2', 'NOVEL', 2020, 'NXB Kim Dong', 2, '2020-02-01', 'NEW')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (3, 'Book name 3', 'NOVEL', 2020, 'NXB Kim Dong', 3, '2020-02-01', 'NEW')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (4, 'Book name 4', 'NOVEL', 2020, 'NXB Kim Dong', 4, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (5, 'Book name 5', 'NOVEL', 2020, 'NXB Kim Dong', 5, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (6, 'Book name 6', 'NOVEL', 2020, 'NXB Kim Dong', 6, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (7, 'Book name 7', 'NOVEL', 2020, 'NXB Kim Dong', 7, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (8, 'Book name 8', 'NOVEL', 2020, 'NXB Kim Dong', 8, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (9, 'Book name 9', 'NOVEL', 2020, 'NXB Kim Dong', 9, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (10, 'Book name 10', 'NOVEL', 2020, 'NXB Kim Dong', 10, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (11, 'Book name 11', 'NOVEL', 2020, 'NXB Kim Dong', 11, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (12, 'Book name 12', 'NOVEL', 2020, 'NXB Kim Dong', 12, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (13, 'Book name 13', 'NOVEL', 2020, 'NXB Kim Dong', 13, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (14, 'Book name 14', 'NOVEL', 2020, 'NXB Kim Dong', 14, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (15, 'Book name 15', 'NOVEL', 2020, 'NXB Kim Dong', 15, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (16, 'Book name 16', 'NOVEL', 2020, 'NXB Kim Dong', 16, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (17, 'Book name 17', 'NOVEL', 2020, 'NXB Kim Dong', 17, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (18, 'Book name 18', 'NOVEL', 2020, 'NXB Kim Dong', 18, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (19, 'Book name 19', 'NOVEL', 2020, 'NXB Kim Dong', 19, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (20, 'Book name 20', 'NOVEL', 2020, 'NXB Kim Dong', 20, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (21, 'Book name 21', 'NOVEL', 2020, 'NXB Kim Dong', 21, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (22, 'Book name 22', 'NOVEL', 2020, 'NXB Kim Dong', 22, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (23, 'Book name 23', 'NOVEL', 2020, 'NXB Kim Dong', 23, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (24, 'Book name 24', 'NOVEL', 2020, 'NXB Kim Dong', 24, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (25, 'Book name 25', 'NOVEL', 2020, 'NXB Kim Dong', 25, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (26, 'Book name 26', 'NOVEL', 2020, 'NXB Kim Dong', 26, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (27, 'Book name 27', 'NOVEL', 2020, 'NXB Kim Dong', 27, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (28, 'Book name 28', 'NOVEL', 2020, 'NXB Kim Dong', 28, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (29, 'Book name 29', 'NOVEL', 2020, 'NXB Kim Dong', 29, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (30, 'Book name 30', 'NOVEL', 2020, 'NXB Kim Dong', 30, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (31, 'Book name 31', 'NOVEL', 2020, 'NXB Kim Dong', 31, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (32, 'Book name 32', 'NOVEL', 2020, 'NXB Kim Dong', 32, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (33, 'Book name 33', 'NOVEL', 2020, 'NXB Kim Dong', 33, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (34, 'Book name 34', 'NOVEL', 2020, 'NXB Kim Dong', 34, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (35, 'Book name 35', 'NOVEL', 2020, 'NXB Kim Dong', 35, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (36, 'Book name 36', 'NOVEL', 2020, 'NXB Kim Dong', 36, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (37, 'Book name 37', 'NOVEL', 2020, 'NXB Kim Dong', 37, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (38, 'Book name 38', 'NOVEL', 2020, 'NXB Kim Dong', 38, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (39, 'Book name 39', 'NOVEL', 2020, 'NXB Kim Dong', 39, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (40, 'Book name 40', 'NOVEL', 2020, 'NXB Kim Dong', 40, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (41, 'Book name 41', 'NOVEL', 2020, 'NXB Kim Dong', 41, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (42, 'Book name 42', 'NOVEL', 2020, 'NXB Kim Dong', 42, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (43, 'Book name 43', 'NOVEL', 2020, 'NXB Kim Dong', 43, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (44, 'Book name 44', 'NOVEL', 2020, 'NXB Kim Dong', 44, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (45, 'Book name 45', 'NOVEL', 2020, 'NXB Kim Dong', 45, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (46, 'Book name 46', 'NOVEL', 2020, 'NXB Kim Dong', 46, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (47, 'Book name 47', 'NOVEL', 2020, 'NXB Kim Dong', 47, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (48, 'Book name 48', 'NOVEL', 2020, 'NXB Kim Dong', 48, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (49, 'Book name 49', 'NOVEL', 2020, 'NXB Kim Dong', 49, '2020-02-01', 'USING')
INSERT [dbo].[book] ([id], [name], [book_type], [publish_year], [publisher], [author_id], [buy_date], [status])
VALUES (50, 'Book name 50', 'NOVEL', 2020, 'NXB Kim Dong', 50, '2020-02-01', 'USING')



