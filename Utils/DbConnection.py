import pyodbc


class DbConnection:
    def __init__(self, drive: str = 'SQL Server', server: str = 'DESKTOP-9GH7QQ6\\SQLEXPRESS', db: str = 'QLTV',
                 username: str = 'sa', password: str = '123456789'):
        self.__drive = drive
        self.__server = server
        self.__db = db
        self.__username = username
        self.__password = password
        self.connection = self.create_connection()

    def create_connection(self) -> pyodbc.Connection:
        str_sql = 'DRIVER={0};SERVER={1};DATABASE={2};UID={3};PWD={4}'.format(self.__drive, self.__server, self.__db,
                                                                              self.__username, self.__password)
        try:
            connection = pyodbc.connect(str_sql)
            return connection
        except Exception as e:
            print("Error in connection: ", e)

    def result_set(self, sql):
        if self.connection is None:
            self.create_connection()
        cursor = self.connection.cursor()
        try:
            cursor.execute(sql)
            rs = cursor.fetchall()
            cursor.close()
            return rs
        except Exception as e:
            print("Error in sql execute: ", e)
            cursor.close()
            return []

    def query(self, sql):
        if self.connection is None:
            self.create_connection()
        cursor = self.connection.cursor()
        try:
            cursor.execute(sql)
            cursor.commit()
            cursor.close()
            return 1
        except Exception as e:
            print("Error in sql execute: ", e)
            cursor.close()
            return 0
