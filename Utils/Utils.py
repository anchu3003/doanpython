from PyQt5.QtWidgets import QMessageBox


class MyUtil:
    @staticmethod
    def showMessage(msg: str):
        dialog = QMessageBox()
        dialog.setWindowTitle('Thông báo')
        dialog.setText(msg)
        dialog.exec_()
