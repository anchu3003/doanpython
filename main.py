import sys

from PyQt5 import QtWidgets

from Controller.AuthenController import AuthenController
from Controller.LoginController import LoginController
from GUI.LoginGUI import Ui_Login

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    LoginWindow = QtWidgets.QMainWindow()
    model = AuthenController()
    controller = LoginController(view=None, model=model, LoginWindow=LoginWindow)
    view = Ui_Login(controller=controller)
    controller.view = view
    view.setupUi(LoginWindow)
    LoginWindow.show()
    sys.exit(app.exec_())
