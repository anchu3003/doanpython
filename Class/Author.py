class Author:
    def __init__(self, Id: int = None, name: str = None):
        self.__id = Id
        self.__name = name

    def __iter__(self):
        yield self.__id
        yield self.__name

    def __str__(self):
        return str([self.__id, self.__name])

    @property
    def id(self) -> int:
        return self.__id

    @id.setter
    def id(self, Id: int):
        self.__id = Id

    @property
    def name(self) -> str:
        return self.__name

    @name.setter
    def name(self, name: str):
        self.__name = name
