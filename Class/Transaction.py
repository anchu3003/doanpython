from Enum.TransactionStatus import TransactionStatus


class Transaction:
    def __init__(self, Id: int = None, memberId: int = None,
                 borrowedDate: str = None,
                 returnDate: str = None,
                 status: TransactionStatus = None,
                 bookIds=None):
        self.__id = Id
        self.__membertId = memberId
        self.__borrowedDate = borrowedDate
        self.__returnDate = returnDate
        self.__status = status
        self.__bookIds = bookIds

    def __str__(self):
        return str([self.__id, self.__bookId, self.__membertId, self.__status])

    @property
    def id(self) -> int:
        return self.__id

    @id.setter
    def id(self, Id: int):
        self.__id = Id

    @property
    def bookIds(self) -> [int]:
        return self.__bookIds

    @bookIds.setter
    def bookIds(self, bookIds: [int]):
        self.__bookIds = bookIds

    @property
    def accountId(self) -> int:
        return self.__membertId

    @accountId.setter
    def accountId(self, accountId: int):
        self.__membertId = accountId

    @property
    def borrowedDate(self) -> str:
        return self.__borrowedDate

    @borrowedDate.setter
    def borrowedDate(self, borrowedDate: str):
        self.__borrowedDate = borrowedDate

    @property
    def returnDate(self) -> str:
        return self.__returnDate

    @returnDate.setter
    def returnDate(self, returnDate: str):
        self.__returnDate = returnDate

    @property
    def status(self) -> TransactionStatus:
        return self.__status

    @status.setter
    def status(self, status: TransactionStatus):
        self.__status = status
