class Member:
    def __init__(self, Id: int = None,
                 name: str = None,
                 birthday: str = None,
                 address: str = None,
                 email: str = None,
                 phoneNo: str = None,
                 ngayLapThe: str = None,
                 debt: int = None):
        self.__id = Id
        self.__name = name
        self.__birthday = birthday
        self.__address = address
        self.__email = email
        self.__phoneNo = phoneNo
        self.__ngayLapThe = ngayLapThe
        self.__debt = debt

    def __str__(self):
        return str([self.__id, self.__name])

    @property
    def id(self) -> int:
        return self.__id

    @id.setter
    def id(self, Id: int):
        self.__id = Id

    @property
    def name(self) -> str:
        return self.__name

    @name.setter
    def name(self, name: str):
        self.__name = name

    @property
    def birthday(self) -> str:
        return self.__birthday

    @birthday.setter
    def birthday(self, birthday: str):
        self.__birthday = birthday

    @property
    def address(self) -> str:
        return self.__address

    @address.setter
    def address(self, address: str):
        self.__address = address

    @property
    def email(self) -> str:
        return self.__email

    @email.setter
    def email(self, email: str):
        self.__email = email

    @property
    def phoneNo(self) -> str:
        return self.__phoneNo

    @phoneNo.setter
    def phoneNo(self, phoneNo: str):
        self.__phoneNo = phoneNo

    @property
    def ngayLapThe(self) -> str:
        return self.__ngayLapThe

    @ngayLapThe.setter
    def ngayLapThe(self, ngayLapThe: str):
        self.__ngayLapThe = ngayLapThe

    @property
    def debt(self) -> int:
        return self.__debt

    @debt.setter
    def debt(self, debt: int):
        self.__debt = debt
