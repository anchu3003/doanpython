from Enum.BookStatus import BookStatus
from Enum.BookType import BookType


class Book:
    def __init__(self, Id: int = None, name: str = None,
                 bookType: BookType = None,
                 publishYear: int = None,
                 publisher: int = None,
                 authorId: int = None,
                 buyDate: str = None,
                 status: BookStatus = None):
        self.__id = Id
        self.__name = name
        self.__bookType = bookType
        self.__publishYear = publishYear
        self.__publisher = publisher
        self.__authorId = authorId
        self.__buyDate = buyDate
        self.__status = status

    def __iter__(self):
        yield self.__id
        yield self.__name
        yield self.__bookType
        yield self.__publishYear
        yield self.__publisher
        yield self.__authorId
        yield self.__buyDate
        yield self.__status

    def __str__(self):
        return str([self.__id, self.__name, self.__bookType, self.__publishYear, self.__publisher, self.__authorId,
                    self.__buyDate, self.__status])

    @property
    def id(self) -> int:
        return self.__id

    @id.setter
    def id(self, Id: int):
        self.__id = Id

    @property
    def name(self) -> str:
        return self.__name

    @name.setter
    def name(self, name: str):
        self.__name = name

    @property
    def bookType(self) -> BookType:
        return self.__bookType

    @bookType.setter
    def bookType(self, bookType: BookType):
        self.__bookType = bookType

    @property
    def publishYear(self) -> int:
        return self.__publishYear

    @publishYear.setter
    def publishYear(self, publishYear: int):
        self.__publishYear = publishYear

    @property
    def buyDate(self) -> str:
        return self.__buyDate

    @buyDate.setter
    def buyDate(self, buyDate: str):
        self.__buyDate = buyDate

    @property
    def publisher(self) -> str:
        return self.__publisher

    @publisher.setter
    def publisher(self, publisher: str):
        self.__publisher = publisher

    @property
    def authorId(self) -> int:
        return self.__authorId

    @authorId.setter
    def authorId(self, authorId: int):
        self.__authorId = authorId

    @property
    def status(self) -> BookStatus:
        return self.__status

    @status.setter
    def status(self, status: BookStatus):
        self.__status = status
