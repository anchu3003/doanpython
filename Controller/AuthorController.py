from Class.Author import Author
from Controller.AuthoriseController import AuthoriseController
from Enum.Permission import Permission
from GUI.SuperGUI import SuperGUI
from Repository.AuthorRepository import AuthorRepository
from Utils.Utils import MyUtil


class AuthorController:
    authorRepository = AuthorRepository()

    def __init__(self, view: SuperGUI):
        self.view = view

    def createAuthor(self) -> bool | str:
        id = self.view.lineEdit_64.text()
        name = self.view.lineEdit_65.text()
        author = Author(id, name)
        if AuthoriseController.checkPermission(Permission.LIBERIAN.value):
            if AuthorController.authorRepository.insert(author):
                MyUtil.showMessage('Bạn đã thêm tác giả mới thành công!')
                self.view.loadDataAuthors()
                return True
            else:
                MyUtil.showMessage('Thêm tác giả thất bại!')
        else:
            return False

    def updateAuthor(self) -> bool | str:
        id = self.view.lineEdit_64.text()
        name = self.view.lineEdit_65.text()
        author = Author(id, name)
        if AuthoriseController.checkPermission(Permission.LIBERIAN.value):
            if AuthorController.authorRepository.update(author):
                MyUtil.showMessage('Update thông tin tác giả thành công!')
                self.view.loadDataAuthors()
                return True
            else:
                MyUtil.showMessage('Update thông tin tác giả thất bại!')
        else:
            return "No Permission"

    def deleteAuthor(self) -> bool | str:
        if AuthoriseController.checkPermission(Permission.LIBERIAN.value):
            id = int(self.view.lineEdit_64.text())
            if id >= 0:
                if AuthorController.authorRepository.delete(id):
                    MyUtil.showMessage('Delete thành công!')
                    self.view.loadDataAuthors()
                    return True
                else:
                    MyUtil.showMessage('Delete thất bại!')
                    return False
        else:
            return "No Permission"

    # @staticmethod
    # def getAuthor(Id: int) -> Author | None | str:
    #     if AuthoriseController.checkPermission(Permission.LIBERIAN.value):
    #         return AuthorController.authorRepository.get(Id)
    #     else:
    #         return "No Permission"

    def getAllAuthor(self, filters: Author = None) -> []:
        if AuthoriseController.checkPermission(Permission.LIBERIAN.value):
            if filters:
                return AuthorController.authorRepository.getAll(filters)
            else:
                id = self.view.lineEdit_64.text()
                name = self.view.lineEdit_65.text()
                author = Author(id, name)
                return AuthorController.authorRepository.getAll(author)
        else:
            return "No Permission"

    def getAllNameAuthor(self) -> []:
        if AuthoriseController.checkPermission(Permission.LIBERIAN.value):
            return AuthorController.authorRepository.getAllAuthorName()
        else:
            return "No Permission"

    def clearAuthor(self):
        self.view.lineEdit_64.clear()
        self.view.lineEdit_65.clear()
