from Controller.AuthenController import AuthenController
from Controller.AuthoriseController import AuthoriseController
from GUI.LoginGUI import Ui_Login
from Utils.Utils import MyUtil


class LoginController:
    def __init__(self, view: Ui_Login, model: AuthenController, LoginWindow):
        self.view = view
        self.model = model
        self.LoginUI = LoginWindow

    def login(self):
        accountID = self.view.accountIDField.text()
        password = self.view.passField.text()
        result = self.model.login(accountID, password)
        if result == True:

            if self.LoginUI:
                self.LoginUI.close()
            self.view.openSuperGUI()
        elif result == False:
            MyUtil.showMessage('Tài khoản không tồn tại hoặc sai password')
