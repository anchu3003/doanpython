from datetime import datetime

from Class.Member import Member
from Controller.AuthoriseController import AuthoriseController
from Enum.Permission import Permission
from GUI.SuperGUI import SuperGUI
from Repository.MemberRepository import MemberRepository
from Utils.Utils import MyUtil


class MemberController:
    memberRepository = MemberRepository()

    def __init__(self, view: SuperGUI):
        self.view = view

    def createMember(self) -> bool | str:
        id = self.view.lineEdit_52.text()
        name = self.view.lineEdit_47.text()
        address = self.view.lineEdit_48.text()
        email = self.view.lineEdit_49.text()
        phoneNo = self.view.lineEdit_51.text()
        debt = self.view.lineEdit_50.text()
        birthday = self.view.dateEdit_8.date().toString('yyyy-MM-dd')
        ngayLapThe = self.view.dateEdit_9.date().toString('yyyy-MM-dd')
        member = Member(id, name, birthday, address, email, phoneNo, ngayLapThe, debt)
        if AuthoriseController.checkPermission(Permission.LIBERIAN.value, Permission.ADMINISTRATOR.value):
            if MemberController.memberRepository.insert(member):
                MyUtil.showMessage('Bạn đã thêm độc giả mới thành công!')
                self.view.loadDataMembers()
                return True
            else:
                MyUtil.showMessage('Thêm độc giả mới không thành công')
        else:
            return False

    def updateMember(self) -> bool | str:
        id = self.view.lineEdit_52.text()
        name = self.view.lineEdit_47.text()
        address = self.view.lineEdit_48.text()
        email = self.view.lineEdit_49.text()
        phoneNo = self.view.lineEdit_51.text()
        debt = self.view.lineEdit_50.text()
        birthday = self.view.dateEdit_8.date().toString('yyyy-MM-dd')
        ngayLapThe = self.view.dateEdit_9.date().toString('yyyy-MM-dd')
        member = Member(id, name, birthday, address, email, phoneNo, ngayLapThe, debt)
        if AuthoriseController.checkPermission(Permission.LIBERIAN.value, Permission.ADMINISTRATOR.value):
            if MemberController.memberRepository.update(member):
                MyUtil.showMessage('Update thông tin độc giả thành công!')
                self.view.loadDataMembers()
                return True
        else:
            return False

    def deleteMember(self) -> bool | str:
        if AuthoriseController.checkPermission(Permission.LIBERIAN.value, Permission.ADMINISTRATOR.value):
            id = int(self.view.lineEdit_52.text())
            if id >= 0:
                if MemberController.memberRepository.delete(id):
                    MyUtil.showMessage('Delete độc giả thành công!')
                    self.view.loadDataMembers()
                    return True
                else:
                    MyUtil.showMessage('Delete thất bại!')
                    return False
        else:
            return "No Permission"

    # @staticmethod
    # def getMember(Id: int) -> Member | None | str:
    #     if AuthoriseController.checkPermission(Permission.LIBERIAN.value, Permission.ADMINISTRATOR.value):
    #         return MemberController.memberRepository.get(Id)
    #     else:
    #         return "No Permission"

    def getAllMember(self, filters: Member = None) -> []:
        if AuthoriseController.checkPermission(Permission.LIBERIAN.value, Permission.ADMINISTRATOR.value):
            if filters:
                return MemberController.memberRepository.getAll(filters)
            else:
                id = self.view.lineEdit_52.text()
                name = self.view.lineEdit_47.text()
                address = self.view.lineEdit_48.text()
                email = self.view.lineEdit_49.text()
                phoneNo = self.view.lineEdit_51.text()
                debt = self.view.lineEdit_50.text()
                birthday = self.view.dateEdit_8.date().toString('yyyy-MM-dd')
                ngayLapThe = self.view.dateEdit_9.date().toString('yyyy-MM-dd')
                member = Member(id, name, birthday, address, email, phoneNo, ngayLapThe, debt)
                return MemberController.memberRepository.getAll(member)
        else:
            return "No Permission"

    def clearMember(self):
        self.view.lineEdit_52.clear()
        self.view.lineEdit_47.clear()
        self.view.lineEdit_48.clear()
        self.view.lineEdit_49.clear()
        self.view.lineEdit_50.clear()
        self.view.lineEdit_51.clear()
        self.view.dateEdit_8.setDate(datetime.strptime('15-01-1999', "%Y-%m-%d"))
        self.view.dateEdit_9.setDate(datetime.strptime('15-01-1999', "%Y-%m-%d"))
