from Controller.AuthoriseController import AuthoriseController
from Utils.DbConnection import DbConnection


class AuthenController:
    connection = DbConnection()

    @staticmethod
    def login(accountId: int, password: str) -> bool:
        # AuthoriseController.perm = 1
        # return True
        try:
            sql = """select * from account where id = {0} and password = {1}""".format(
                accountId, password)
            valid = AuthenController.connection.result_set(sql)
            if bool(valid):
                AuthoriseController.perm = int(valid[0][7])
                return True
            else:
                return False
        except Exception as e:
            print("login error")
