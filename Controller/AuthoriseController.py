from Enum.Permission import Permission


class AuthoriseController:
    perm = None

    @staticmethod
    def checkPermission(*permission: Permission) -> bool:
        if AuthoriseController.perm:
            if permission.__contains__(AuthoriseController.perm) or AuthoriseController.perm == 1:
                return True
        return False
