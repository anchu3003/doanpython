from Class.Transaction import Transaction
from Controller.AuthoriseController import AuthoriseController
from Enum.Permission import Permission
from Repository.TransactionRepository import TransactionRepository
from Repository.BookRepository import BookRepository
from Utils.Utils import MyUtil
from GUI.SuperGUI import SuperGUI
from Enum.TransactionStatus import TransactionStatus


class TransactionController:
    libTransactionRepository = TransactionRepository()

    def __init__(self, viewSys: SuperGUI):
        self.viewSys = viewSys


    def createLibTransaction(self, transaction) -> bool | str:
        if AuthoriseController.checkPermission(Permission.LIBERIAN.value):
            if TransactionController.libTransactionRepository.insert(transaction):
                MyUtil.showMessage('Bạn đã tạo giao dịch thành công!')
                self.viewSys.loadDataTransactions()
                return True
            else:
                MyUtil.showMessage('Tạo giao dịch thất bại!')
        else:
            return False

    def updateLibTransaction(self, modify: Transaction) -> bool | str:
        if AuthoriseController.checkPermission(Permission.LIBERIAN.value):
            if TransactionController.libTransactionRepository.update(modify):
                MyUtil.showMessage('Bạn đã update giao dịch thành công!')
                self.viewSys.loadDataTransactions()
                return True
            else:
                MyUtil.showMessage('Bạn đã update giao dịch thất bại!')
        else:
            return "No Permission"
    #
    # @staticmethod
    # def deleteLibTransaction(Id: int) -> bool | str:
    #     if AuthoriseController.checkPermission(Permission.LIBERIAN.value):
    #         return TransactionController.libTransactionRepository.delete(Id)
    #     else:
    #         return "No Permission"

    @staticmethod
    def getLibTransaction(Id: int) -> Transaction | None | str:
        if AuthoriseController.checkPermission(Permission.LIBERIAN.value):
            return TransactionController.libTransactionRepository.getByName(Id)
        else:
            return "No Permission"

    @staticmethod
    def getAllLibTransaction(filters: Transaction = None) -> []:
        if AuthoriseController.checkPermission(Permission.LIBERIAN.value):
            return TransactionController.libTransactionRepository.getAll(filters)
        else:
            return "No Permission"
