from Class.Book import Book
from Controller.AuthoriseController import AuthoriseController
from Enum.BookStatus import BookStatus
from Enum.BookType import BookType
from Enum.Permission import Permission
from GUI.SuperGUI import SuperGUI
from Repository.AuthorRepository import AuthorRepository
from Repository.BookRepository import BookRepository
from Utils.Utils import MyUtil


class BookController:
    bookRepository = BookRepository()

    def __init__(self, view: SuperGUI):
        self.view = view

    def createBook(self) -> bool | str:
        id = self.view.lineEdit_59.text()
        bookName = self.view.lineEdit_60.text()
        nameAuthor = self.view.cbbAuthorNameList.currentText()
        bookTypeValue = self.view.cbbBookType.currentText()
        publishYear = self.view.lineEdit_66.text()
        publisher = self.view.lineEdit_63.text()
        buyDate = self.view.dateEdit_13.date().toString('yyyy-MM-dd')
        bookSatusValue = self.view.cbbBookSatus.currentText()

        a = AuthorRepository()
        authorID = int(a.getByName(nameAuthor).id)

        bookStatus = next(member for member in BookStatus if member.value == bookSatusValue)
        bookType = next(type for type in BookType if type.value == bookTypeValue)

        book = Book(id, bookName, bookType.name, publishYear, publisher, authorID, buyDate, bookStatus.name)
        if AuthoriseController.checkPermission(Permission.LIBERIAN.value):
            if BookController.bookRepository.insert(book):
                MyUtil.showMessage('Bạn đã thêm sách mới thành công!')
                self.view.loadDataBooks()
                return True
            else:
                MyUtil.showMessage('Thêm sách thất bại!')
        else:
            return False

    def updateBook(self) -> bool | str:
        id = self.view.lineEdit_59.text()
        bookName = self.view.lineEdit_60.text()
        nameAuthor = self.view.cbbAuthorNameList.currentText()
        bookTypeValue = self.view.cbbBookType.currentText()
        publishYear = self.view.lineEdit_66.text()
        publisher = self.view.lineEdit_63.text()
        buyDate = self.view.dateEdit_13.date().toString('yyyy-MM-dd')
        bookSatusValue = self.view.cbbBookSatus.currentText()
        a = AuthorRepository()
        authorID = int(a.getByName(nameAuthor).id)

        bookStatus = next(member for member in BookStatus if member.value == bookSatusValue)
        bookType = next(type for type in BookType if type.value == bookTypeValue)

        book = Book(id, bookName, bookType.name, publishYear, publisher, authorID, buyDate, bookStatus.name)
        if AuthoriseController.checkPermission(Permission.LIBERIAN.value):
            if BookController.bookRepository.update(book):
                MyUtil.showMessage('Update thông tin sách thành công!')
                self.view.loadDataBooks()
                return True
            else:
                MyUtil.showMessage('Update thất bại!')
        else:
            return "No Permission"

    def deleteBook(self) -> bool | str:
        if AuthoriseController.checkPermission(Permission.LIBERIAN.value):
            id = int(self.view.lineEdit_59.text())
            if id >= 0:
                if BookController.bookRepository.delete(id):
                    MyUtil.showMessage('Delete thành công!')
                    self.view.loadDataBooks()
                    return True
                else:
                    MyUtil.showMessage('Delete thất bại!')
                    return False
        else:
            return "No Permission"

    # @staticmethod
    # def getBook(Id: int) -> Book | None | str:
    #     if AuthoriseController.checkPermission(Permission.LIBERIAN.value):
    #         return BookController.bookRepository.get(Id)
    #     else:
    #         return "No Permission"

    def getAllBook(self, filters: Book = None) -> []:
        if AuthoriseController.checkPermission(Permission.LIBERIAN.value):
            if filters:
                return BookController.bookRepository.getAll(filters)
            else:
                id = self.view.lineEdit_59.text()
                # password = self.view.lineEdit_3.text()
                name = self.view.lineEdit_60.text()
                # address = self.view.lineEdit_5.text()
                # birthday = self.view.dateEdit.date().toString('yyyy-MM-dd')
                # email = self.view.lineEdit_6.text()
                # phoneNo = self.view.lineEdit_7.text()
                book = Book(id, name)
                return BookController.bookRepository.getAll(book)
        else:
            return "No Permission"

    def clearBook(self):
        self.view.lineEdit_59.clear()
        self.view.lineEdit_60.clear()
        self.view.lineEdit_66.clear()
        self.view.lineEdit_63.clear()
