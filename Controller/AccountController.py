from datetime import datetime

from Class.Account import Account
from Controller.AuthoriseController import AuthoriseController
from Enum.Permission import Permission
from GUI.SuperGUI import SuperGUI
from Repository.AccountRepository import AccountRepository
from Utils.Utils import MyUtil


class AccountController:
    accountRepository = AccountRepository()

    def __init__(self, view: SuperGUI):
        self.view = view

    def createAdmin(self) -> bool | str:
        id = self.view.lineEdit_2.text()
        password = self.view.lineEdit_3.text()
        name = self.view.lineEdit_4.text()
        address = self.view.lineEdit_5.text()
        birthday = self.view.dateEdit.date().toString('yyyy-MM-dd')
        email = self.view.lineEdit_6.text()
        phoneNo = self.view.lineEdit_7.text()
        permission = 2
        account = Account(id, password, name, birthday, address, email, phoneNo, permission)
        if AuthoriseController.checkPermission(Permission.LIBERIAN.value, Permission.ADMINISTRATOR.value):
            if AccountController.accountRepository.insert(account):
                MyUtil.showMessage('Bạn đã tạo tài khoản thành công!')
                self.view.loadDataAdmins()
                return True
            else:
                MyUtil.showMessage('Tạo tài khoản thất bại!')
        else:
            return False

    def createLiberian(self) -> bool | str:
        id = self.view.lineEdit_14.text()
        password = self.view.lineEdit_15.text()
        name = self.view.lineEdit_16.text()
        address = self.view.lineEdit_17.text()
        birthday = self.view.dateEdit_3.date().toString('yyyy-MM-dd')
        email = self.view.lineEdit_18.text()
        phoneNo = self.view.lineEdit_19.text()
        permission = 3
        account = Account(id, password, name, birthday, address, email, phoneNo, permission)
        if AuthoriseController.checkPermission(Permission.LIBERIAN.value, Permission.ADMINISTRATOR.value):
            if AccountController.accountRepository.insert(account):
                MyUtil.showMessage('Bạn đã tạo tài khoản thủ thư thành công!')
                self.view.loadDataLiberians()
                return True
            else:
                MyUtil.showMessage('Tạo tài khoản thất bại!')
        else:
            return False

    def updateAdmin(self) -> bool | str:
        id = self.view.lineEdit_2.text()
        password = self.view.lineEdit_3.text()
        name = self.view.lineEdit_4.text()
        address = self.view.lineEdit_5.text()
        birthday = self.view.dateEdit.date().toString('yyyy-MM-dd')
        email = self.view.lineEdit_6.text()
        phoneNo = self.view.lineEdit_7.text()
        account = Account(id, password, name, birthday, address, email, phoneNo)
        if AuthoriseController.checkPermission(Permission.LIBERIAN.value, Permission.ADMINISTRATOR.value):
            if AccountController.accountRepository.update(account):
                MyUtil.showMessage('Update thông tin admin thành công!')
                self.view.loadDataAdmins()
                return True
            else:
                MyUtil.showMessage('Update thất bại!')
        else:
            return "No Permission"

    def updateLiberian(self) -> bool | str:
        id = self.view.lineEdit_14.text()
        password = self.view.lineEdit_15.text()
        name = self.view.lineEdit_16.text()
        address = self.view.lineEdit_17.text()
        birthday = self.view.dateEdit_3.date().toString('yyyy-MM-dd')
        email = self.view.lineEdit_18.text()
        phoneNo = self.view.lineEdit_19.text()
        account = Account(id, password, name, birthday, address, email, phoneNo)
        if AuthoriseController.checkPermission(Permission.LIBERIAN.value, Permission.ADMINISTRATOR.value):
            if AccountController.accountRepository.update(account):
                MyUtil.showMessage('Update thành công!')
                self.view.loadDataLiberians()
                return True
            else:
                MyUtil.showMessage('Update thất bại!')
        else:
            return "No Permission"

    def deleteAdmin(self) -> bool | str:
        if AuthoriseController.checkPermission(Permission.LIBERIAN.value, Permission.ADMINISTRATOR.value):
            id = int(self.view.lineEdit_2.text())
            if id >= 0:
                if AccountController.accountRepository.delete(id):
                    MyUtil.showMessage('Delete thành công!')
                    self.view.loadDataAdmins()
                    return True
                else:
                    MyUtil.showMessage('Delete thất bại!')
                    return False
        else:
            return "No Permission"

    def deleteLiberian(self) -> bool | str:
        if AuthoriseController.checkPermission(Permission.LIBERIAN.value, Permission.ADMINISTRATOR.value):
            id = int(self.view.lineEdit_14.text())
            if id >= 0:
                if AccountController.accountRepository.delete(id):
                    MyUtil.showMessage('Delete thành công!')
                    self.view.loadDataLiberians()
                    return True
                else:
                    MyUtil.showMessage('Delete thất bại!')
                    return False
        else:
            return "No Permission"

    # def getAccount(self, Id: int) -> Account | None | str:
    #     if AuthoriseController.checkPermission(Permission.LIBERIAN.value, Permission.ADMINISTRATOR.value):
    #         return AccountController.accountRepository.get(Id)
    #     else:
    #         return "No Permission"

    def getAllAccount(self, filters: Account = None) -> []:
        if AuthoriseController.checkPermission(Permission.LIBERIAN.value, Permission.ADMINISTRATOR.value):
            if filters:
                return AccountController.accountRepository.getAll(filters)
            else:
                id = self.view.lineEdit_2.text()
                password = self.view.lineEdit_3.text()
                name = self.view.lineEdit_4.text()
                address = self.view.lineEdit_5.text()
                birthday = self.view.dateEdit.date().toString('yyyy-MM-dd')
                email = self.view.lineEdit_6.text()
                phoneNo = self.view.lineEdit_7.text()
                account = Account(id, password, name, address, birthday, email, phoneNo)
                return AccountController.accountRepository.getAll(account)
        else:
            return "No Permission"

    def clearAdmin(self):
        self.view.lineEdit_2.clear()
        self.view.lineEdit_3.clear()
        self.view.lineEdit_4.clear()
        self.view.lineEdit_5.clear()
        self.view.dateEdit.setDate(datetime.strptime('15-01-1999', "%Y-%m-%d"))
        self.view.lineEdit_6.clear()
        self.view.lineEdit_7.clear()

    def clearLiberian(self):
        self.view.lineEdit_14.clear()
        self.view.lineEdit_15.clear()
        self.view.lineEdit_16.clear()
        self.view.lineEdit_17.clear()
        self.view.dateEdit_3.setDate(datetime.strptime('15-01-1999', "%Y-%m-%d"))
        self.view.lineEdit_18.clear()
        self.view.lineEdit_19.clear()
